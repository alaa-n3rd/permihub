<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
{!! html::script('public/js/jquery.min.js') !!}
{!! html::script('public/js/global.js') !!}
{!! html::script('public/js/bootstrap.min.js') !!}
{!! html::script('public/js/bootstrap3-typeahead.min.js')!!}
{!! html::script('public/js/jquery.mask.js') !!}
{!! html::script('public/js/docs.js') !!}
{!! html::script('public/js/jquery.number.min.js') !!}
{!! html::script('public/js/fileinput.min.js') !!}
{!! html::script('public/js/iban.js') !!}
{!! html::script('public/js/croppie.js') !!}
{{-- <script src="http://demo.itsolutionstuff.com/plugin/croppie.js"></script> --}}


<!-- script scroll -->
{{--<script type="text/javascript">
    function CheckUserSession() {
        var userInSession = false;
        var path = "{{route('check_session')}}";
        $.ajax({
            url: path,// It will return true/false based on session
            type: 'GET',
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            async: false,
            success: function (result) {
                console.log(result)
                userInSession = result;
            }
        });
        return userInSession;
    }
    function CheckUsersSession() {
        var userInSession = false;
        var paths = "{{route('check_sessions')}}";
        $.ajax({
            url: paths,// It will return true/false based on session
            type: 'GET',
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            async: false,
            success: function (result) {
                console.log(result)
                userInSession = result;
            }
        });
        return userInSession;
    }

</script>--}}
{{--<script type="text/javascript">
    $(document).ready(function() {
        // navigation click actions
        $('.scroll-link').on('click', function(event){
            event.preventDefault();
            var sectionID = $(this).attr("data-id");
            scrollToID('#' + sectionID, 750);
        });
        // scroll to top action
        $('.scroll-top').on('click', function(event) {
            event.preventDefault();
            $('html, body').animate({scrollTop:0}, 'slow');
        });
        // mobile nav toggle
        $('#nav-toggle').on('click', function (event) {
            event.preventDefault();
            $('#main-nav').toggleClass("open");
        });
    });
    // scroll function
    function scrollToID(id, speed){
        var offSet = 50;
        var targetOffset = $(id).offset().top - offSet;
        var mainNav = $('#main-nav');
        $('html,body').animate({scrollTop:targetOffset}, speed);
        if (mainNav.hasClass("open")) {
            mainNav.css("height", "1px").removeClass("in").addClass("collapse");
            mainNav.removeClass("open");
        }
    }
    if (typeof console === "undefined") {
        console = {
            log: function() { }
        };
    }
</script>--}}
<!-- end script scroll -->

<script type="text/javascript">
    /*$('#myTabs a').click(function (e) {
        e.preventDefault()
        $(this).tab('show')
    })*/
</script>

{{--<script type="text/javascript">
    var path = "{{route('autcomplete_teacher')}}";
    var pathFR = "{{route('autcomplete_teacher_fr')}}";
    var pathCN = "{{route('autcomplete_teacher_cn')}}";

    $('#name_educator').typeahead({
        displayText: function(item) {
            return item.name
        },
        afterSelect: function(item) {
            if (window.event.keyCode == 9 ) return false;
            this.$element[0].value = item.name
            $('#teacher_id').val(item.id);
            window.location.replace("{{route('home')}}/detail-teacher/"+item.name+"/"+item.id+"");
        },
        source: function (query, process) {
            return $.getJSON(pathFR, { query: query }, function(data) {
                process(data)
            })
        },
    });

    $('#name_educator2').typeahead({
        displayText: function(item) {
            return item.name
        },
        afterSelect: function(item) {
            if (window.event.keyCode == 9 ) return false;
            this.$element[0].value = item.name
            $('#teacher_id').val(item.id);
            console.log("{{route('home')}}/detail-teacher/"+item.name+"/"+item.id+"");
            window.location.replace("{{route('home')}}/detail-teacher/"+item.name+"/"+item.id+"");
        },
        source: function (query, process) {
            return $.getJSON(pathCN, { query: query }, function(data) {
                process(data)
            })
        },
    });
    $('#name_educator_modal').typeahead({
        displayText: function(item) {
            return item.name
        },
        afterSelect: function(item) {
            if (window.event.keyCode == 9 ) return false;
            this.$element[0].value = item.name
            $('#teacher_id').val(item.id);
            console.log("{{route('home')}}/detail-teacher/"+item.name+"/"+item.id+"");
            window.location.replace("{{route('home')}}/detail-teacher/"+item.name+"/"+item.id+"");
        },
        source: function (query, process) {
            return $.getJSON(path, { query: query }, function(data) {
                process(data)
            })
        },
    });


</script>--}}
{{--<script>
    var pathFR = "{{route('autcomplete_teacher_fr')}}";
    var pathCN = "{{route('autcomplete_teacher_cn')}}";
    $('#lang_search').on('change', function() {
        if (this.value == 'fr') {
            path = pathFR;
        }
        else if (this.value == 'cn') {
            path = pathCN;
        }


    });
    $('#name_educator_header').typeahead({
        displayText: function(item) {
            return item.name
        },
        afterSelect: function(item) {
            if (window.event.keyCode == 9 ) return false;
            this.$element[0].value = item.name
            $('#teacher_id').val(item.id);
            window.location.replace("{{route('home')}}/detail-teacher/"+item.name+"/"+item.id+"");
        },
        source: function (query, process) {
            return $.getJSON(path, { query: query }, function(data) {
                process(data)
            })
        },
    });
</script>--}}




{{--<script type="text/javascript">

    $( document ).on( "click", "#submit-form-signup-students", function() { /* /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/ */
        var reg = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;
        var reg1 = /^([^0-9]*)$/;
        var pattern = /^(?:[\w-]¦&#[0-9]{5};){2,16}$/;
        var customers_firstname = $('.form-signup-students #customers_firstname').val();
        var customers_lastname = $('.form-signup-students #customers_lastname').val();
        var customers_email = $('.form-signup-students #customers_email').val();
        var customers_country = $('.form-signup-students #customers_country').val();
        var customers_password = $('.form-signup-students #customers_password').val();
        var confirm_customers_password = $('.form-signup-students #confirm_customers_password').val();
        var rendez_vous_value_signup = $('.form-signup-students #rendez-vous-value-signup').val();
        var customers_program_societe = $('.form-signup-students #customers_program_societe').val();
        var customers_civility = $('.form-signup-students input[name="customers_civility"]:checked').val();
        var customers_lang_origin = $('.form-signup-students #customers_lang_origin').val();
        var type = $('.form-signup-students #type').val();


        var customers_firstname_test = customers_firstname.replace(/ /g, "");
        var customers_lastname_test = customers_lastname.replace(/ /g, "");
        var customers_password_test = customers_password.replace(/ /g, "");
        var confirm_customers_password_test = confirm_customers_password.replace(/ /g, "");
        //alert(customers_lastname);
        if($('.form-signup-students input[name="customers_civility"]:checked').length == ''){
            $('.notification-block').html('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>@lang('common.inscription_error_civility')</div>');
            return false;
        }else{
            $('.form-signup-students input[name="customers_civility"]').css('background-color','#DFF0D8');
        }
        if(customers_lastname == '' || reg1.test(customers_lastname) == false || customers_lastname_test == '' || customers_lastname.length <= 2 || pattern.exec(customers_lastname) == false)
        {
            $('.form-signup-students #customers_lastname').css('background-color','#FBBEBE');
            $('.notification-block').html('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>Veuillez renseigner un nom valide.</div>');
            return false;
        }
        else{
            $('.form-signup-students #customers_lastname').css('background-color','#DFF0D8');
        }

        if(customers_firstname == '' || reg1.test(customers_firstname) == false || customers_firstname_test == '' || customers_firstname.length <= 2 || pattern.exec(customers_firstname) == false)
        {
            $('.form-signup-students #customers_firstname').css('background-color','#FBBEBE');
            $('.notification-block').html('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>Veuillez renseigner votre prénom.</div>');
            return false;
        }
        else{
            $('.form-signup-students #customers_firstname').css('background-color','#DFF0D8');
        }
        if(reg.test(customers_email) == false){
            $('.form-signup-students #customers_email').css('background-color','#FBBEBE');
            $('.notification-block').html('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>Veuillez renseigner votre adresse mail.</div>');
            return false;
        }
        else{
            $('.form-signup-students #customers_email').css('background-color','#DFF0D8');
        }
        if(reg1.test(customers_country) == false || customers_country == ''){
            $('.form-signup-students #customers_country').css('background-color','#FBBEBE');
            $('.notification-block').html('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>@lang('common.inscription_error_country')</div>');
            return false;
        }
        else{
            $('.form-signup-students #customers_country').css('background-color','#DFF0D8');
        }
        if(customers_password == '' || customers_password_test==''){
            $('.form-signup-students #customers_password').css('background-color','#FBBEBE');
            $('.notification-block').html('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>Veuillez choisir un mot de passe.</div>');
            return false;
        }
        else{
            $('.form-signup-students #customers_password').css('background-color','#DFF0D8');
        }
        if(confirm_customers_password == '' || confirm_customers_password_test==''){
            $('.form-signup-students #confirm_customers_password').css('background-color','#FBBEBE');
            $('.notification-block').html('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>Veuillez confirmer votre mot de passe.</div>');
            return false;
        }else{
            $('.form-signup-students #confirm_customers_password').css('background-color','#DFF0D8');
        }
        if (customers_password != confirm_customers_password) {
            $('.form-signup-students #customers_password').css('background-color','#FBBEBE');
            $('.form-signup-students #confirm_customers_password').css('background-color','#FBBEBE');
            $('.notification-block').html('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>@lang('common.inscription_error_identique_password')</div>');
            return false;
        } else {
            $('.form-signup-students #customers_password').css('background-color','#DFF0D8');
            $('.form-signup-students #confirm_customers_password').css('background-color','#DFF0D8');
        }
        if($('.form-signup-students input[name="customers_program_societe"]:checked').length == ''){
            customers_program_societe = '';
        }
        else{
            $('.form-signup-students input[name="customers_program_societe"]').css('background-color','#DFF0D8');
        }
        if($('.form-signup-students input[name="user_age_condition"]:checked').length == ''){
            $('.form-signup-students input[name="user_age_condition"]').css('background-color','#FBBEBE');
            $('.notification-block').html('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>Veuillez confirmer que vous avez plus de 16 ans.</div>');
            return false;
        }
        else{
            $('.form-signup-students input[name="user_age_condition"]').css('background-color','#DFF0D8');
        }
        if($('.form-signup-students input[name="user_condition"]:checked').length == ''){
            $('.form-signup-students input[name="user_condition"]').css('background-color','#FBBEBE');
            $('.notification-block').html('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>@lang('common.inscription_error_condition_general')</div>');
            return false;
        }
        else{
            $('.form-signup-students input[name="user_condition"]').css('background-color','#DFF0D8');
        }


        // save customer student

        $.ajax({
            url : '{{route('ajax_create_account_student')}}',
            type : 'POST',
            data : '_token={{ csrf_token() }}&customers_firstname='+customers_firstname+'&customers_lastname='+customers_lastname+'&customers_email='+customers_email+'&customers_country='+customers_country+'&customers_program_societe='+customers_program_societe+'&customers_password='+customers_password+'&confirm_customers_password='+confirm_customers_password+'&rendez_vous_value_signup='+rendez_vous_value_signup+'&customers_civility='+customers_civility+'&type='+type+'&customers_lang_origin='+customers_lang_origin,
            dataType:"json",
            success : function(jsonData){
                if(jsonData.echec != ''){
                    $('.notification-block').html('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"> × </button> '+jsonData.echec+'</div>');
                }else{
                    $('.notification-block').html('<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"> × </button> '+jsonData.succes+'</div>');
                    setTimeout(function(){
                        if(jsonData.orders_key_url != ''){
                            window.location.href="{{route('home')}}/payment/form/"+jsonData.orders_key_url;
                        }else{
                            window.location.href="{{route('my_courses')}}";
                        }
                    }, 1000);
                }

            },
            error : function(resultat, statut, erreur){

            }
        });
        return false;
    });


    $( document ).on( "click", "#submit-form-signin-students", function() {
        var getUrlParameter = function getUrlParameter(sParam) {
            var sPageURL = decodeURIComponent(window.location.search.substring(1)),
                sURLVariables = sPageURL.split('&'),
                sParameterName,
                i;

            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');

                if (sParameterName[0] === sParam) {
                    return sParameterName[1] === undefined ? true : sParameterName[1];
                }
            }
        };
        var education_type = getUrlParameter('education_type');
        var reg = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;
        var customers_email = $('.form-signin-students #customers_email').val();
        var customers_password = $('.form-signin-students #customers_password').val();
        var rendez_vous_value_signin = $('.form-signin-students #rendez-vous-value-login').val();

        var customers_password_test = customers_password.replace(/ /g, "");

        if(reg.test(customers_email) == false){
            $('.form-signin-students #customers_email').css('background-color','#FBBEBE');
            return false;
        }else if(customers_password == '' || customers_password_test==''){
            $('.form-signin-students #customers_password').css('background-color','#FBBEBE');
            return false;
        }

        // connect customer student

        $.ajax({
            url : '{{route('ajax_connect_student')}}',
            type : 'POST',
            data : '_token={{ csrf_token() }}&customers_email='+customers_email+'&customers_password='+customers_password+'&rendez_vous_value_signin='+rendez_vous_value_signin+'&education_type='+education_type,
            dataType:"json",
            success : function(jsonData){
                if(jsonData.echec != ''){
                    $('.notification_signin-block').html('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"> × </button> '+jsonData.echec+'</div>');
                }else{
                    $('.notification_signin-block').html('<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"> × </button> '+jsonData.succes+'</div>');
                    setTimeout(function(){
                        if(jsonData.orders_key_url != ''){
                            window.location.href="{{route('home')}}/payment/form/"+jsonData.orders_key_url;
                        }else{
                            window.location.href="{{route('my_courses')}}";
                        }
                    }, 1000);
                }

            },
            error : function(resultat, statut, erreur){

            }
        });
        return false;

    });

</script>
<script>
    $( document ).on( "click", "#submit-form-signin-teacher", function() {
        var reg = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;
        var customers_email = $('.form-signin-become-teacher #customers_email').val();
        var customers_password = $('.form-signin-become-teacher #customers_password').val();
        // var rendez_vous_value_signin = $('.form-signin-become-teacher #rendez-vous-value-login').val();

        var customers_password_test = customers_password.replace(/ /g, "");

        if(reg.test(customers_email) == false){
            $('.form-signin-students #customers_email').css('background-color','#FBBEBE');
            return false;
        }else if(customers_password == '' || customers_password_test==''){
            $('.form-signin-students #customers_password').css('background-color','#FBBEBE');
            return false;
        }

        // connect customer student

        $.ajax({
            url : '{{route('login_teacher')}}',
            type : 'POST',
            data : '_token={{ csrf_token() }}&customers_email='+customers_email+'&customers_password='+customers_password,
            dataType:"json",
            success : function(jsonData){
                if(jsonData.echec != ''){
                    $('.notification_teacher').html('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"> × </button> '+jsonData.echec+'</div>');
                }else{
                    $('.notification_teacher').html('<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"> × </button> '+jsonData.succes+'</div>');
                    setTimeout(function(){
                        window.location.href="{{route('calender_teacher')}}";

                    }, 1000);
                }

            },
            error : function(resultat, statut, erreur){

            }
        });
        return false;

    });
</script>
<script>
    $(document).ready(function(){
        $('.panel-title a').on('click', function(){

            // Grab img from clicked question
            var img = $(this).children('img');
            // remove Rotate class from all images except the active
            $('img').not(img).removeClass('rotate');

            // toggle rotate class
            img.toggleClass('rotate');

        });

    });
</script>
--}}{{-- Partners Carousel JS --}}{{--
<script type="text/javascript">
    $(document).ready(function(){

        $('#itemslider').carousel({ interval: 3000 });

        $('.carousel-showmanymoveone .item').each(function(){
            var itemToClone = $(this);

            for (var i=1;i<6;i++) {
                itemToClone = itemToClone.next();

                if (!itemToClone.length) {
                    itemToClone = $(this).siblings(':first');
                }

                itemToClone.children(':first-child').clone()
                    .addClass("cloneditem-"+(i))
                    .appendTo($(this));
            }
        });
    });

</script>
<script>
    $(document).ready(function(){
        if ( $('.block-list-teacher ul li').size() == 0 ) {
            $('.block-list-teacher ul').append('<h3>Aucun enseignant ne correspond à votre recherche.</h3>');
        }

        @if(!empty($session_customers_students) && $current_page !== "home" )
        $(".x-nav").attr("style", "padding-top:140px !important;");
        @endif
    });
</script>--}}
{{--@if($current_page == 'home' || $current_page == 'become_teacher')
    <script>
        $("#header-arc-en-chine").addClass("inactive");
        $('.block-logo img').addClass("img-switcher");
        $('.block-logo img').attr('src', '{{route('home')}}/public/img/logo_blanc.svg');
        $(window).on("scroll", function() {
            if($(window).scrollTop() > 50) {
                $("#header-arc-en-chine").removeClass("inactive");
                $('.block-logo img').removeClass("img-switcher");
                $('.block-logo img').attr('src', '{{route('home')}}/public/img/logo_noir.svg');
                //$(".slide-home").removeClass("disable");
            } else if($(window).scrollTop() < 50){
                //remove the background property so it comes transparent again (defined in your css)
                $("#header-arc-en-chine").addClass("inactive");
                $('.block-logo img').addClass("img-switcher");
                $('.block-logo img').attr('src', '{{route('home')}}/public/img/logo_blanc.svg');
            }
        });
    </script>
@endif--}}
{{--@if(session()->has('signout'))
    <script type="text/javascript">
        $(window).on('load',function(){
            $('#ModalSignOut').modal('show');
        });
    </script>
@endif--}}
{{--<script>
    function ResetPasswordStudent(){
        $('#myModal1').modal('hide');
        $('#ModalForgetPassword').modal('show');
        $('#ModalForgetPassword #submit-reset-password').attr('onclick','valid_email()');
    }
</script>--}}
{{--
<script>
    function valid_email(){
        var type = 'student';
        var reg = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;
        var reg1 = /^([^0-9]*)$/;
        var customers_email = $('#forget-password-form #customers_email').val();
        if(customers_email == ''){
            $('#forget-password-form #customers_email').css('background-color','#FBBEBE');
            $('#forget-password-form label').css('color','red');
            $('#forget-password-form label').html('Veuillez renseigner votre adresse mail.');
            return;
        }else if(reg.test(customers_email) == false){
            $('#forget-password-form #customers_email').css('background-color','#FBBEBE');
            $('#forget-password-form label').css('color','red');
            $('#forget-password-form label').html('Veuillez vérifier le format de votre adresse mail.');
            return false;
        }
        $.ajax({
            url : '{{route('valid_email')}}',
            type : 'get',
            data : 'type='+type+'&email='+customers_email,
            dataType:"json",
            success : function(jsonData){
                console.log(jsonData);
                if (jsonData.succes == true) {
                    $('#forget-password-form label').css('color','green');
                    $('#forget-password-form label').css('font-size','14px');
                    @if(empty(Session::get('applocale')) || Session::get('applocale') == 'fr')
                    $('#forget-password-form label').html('Un nouveau mot de passe a été envoyé à l\'adresse '+customers_email+'');
                    @elseif(Session::get('applocale') == 'cn')
                    $('#forget-password-form label').html('新密码已发到您的电子邮箱 '+customers_email+'');
                    @endif
                        customers_email = '';
                } else if (jsonData.succes == false) {
                    $('#forget-password-form label').css('color','red');
                    $('#forget-password-form label').css('font-size','14px');
                    @if(empty(Session::get('applocale')) || Session::get('applocale') == 'fr')
                    $('#forget-password-form label').html('Votre adresse mail ne correspond à aucun compte connu. Vérifiez que vous tentez bien de vous connecter en tant comme élève si vous être un professeur, et inversement.');
                    @elseif(Session::get('applocale') == 'cn')
                    $('#forget-password-form label').html('此邮件地址不存在！');
                    @endif
                        customers_email = '';
                }

                setTimeout(function(){window.location.href="{{route('home')}}";}, 3000);

            },
            error : function(resultat, statut, erreur){
            }
        });
    }
</script>
<script>
    ('.modal').on('shown.bs.modal', function (e) {
        $('html').addClass('freezePage');
        $('body').addClass('freezePage');
    });
    $('.modal').on('hidden.bs.modal', function (e) {
        $('html').removeClass('freezePage');
        $('body').removeClass('freezePage');
    });
</script>--}}
