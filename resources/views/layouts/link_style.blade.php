
<link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700" rel="stylesheet" type="text/css" media="all">
<link href="https://fonts.googleapis.com/css?family=Raleway:300,400" rel="stylesheet">
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
{!! Html::style('css/global.css') !!}
{!! Html::style('public/css/home.css') !!}
{!! Html::style('public/css/bootstrap.min.css') !!}
{!! Html::style('public/css/font-awesome.min.css') !!}
{!! Html::style('public/css/bootstrap-datepicker.min.css') !!}
{!! Html::style('public/css/responsive.css') !!}
{!! Html::style('public/css/docs.css') !!}
{!! Html::style('public/css/flag-icon.css') !!}
{!! Html::style('public/css/fileinput.css') !!}
<link rel="stylesheet" href="http://demo.itsolutionstuff.com/plugin/croppie.css">

{{-- {!! html::script('public/js/jquery.min.js') !!} --}}

{{-- <script src="public/js/appearin-sdk.0.0.4.min.js"></script> --}}
