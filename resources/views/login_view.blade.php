@extends('template')
@section('contenu')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="logo">
                    {!! Html::image('images/logo_permihub.png', 'Permi hub', array('style' => 'footer-logo')) !!}
                </div>
                <div class="login_form">
                    <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <div class="col-md-offset-3 col-md-6">
                                <input id="email" type="email" class="form-control" name="email"
                                       value="{{ old('email') }}" placeholder="Identifiant" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <div class="col-md-offset-3 col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required
                                       placeholder="Mot de passe">
                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-3">
                                <div class="col-md-12 forgetpassword">
                                    <a class="btn btn-link" href="{{ route('login') }}">
                                        Mot de passe oublié?
                                    </a>
                                </div>
                                <div class="col-md-12 connexion">
                                    <button type="submit" class="btn btn-primary">
                                        Connexion
                                    </button>
                                </div>

                            </div>
                        </div>
                    </form>
                    <div class="create_accout">
                        <a href="{{ route('inscription') }}">Vous n'avez pas de compte ?</a>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
@section('script')

@endsection
