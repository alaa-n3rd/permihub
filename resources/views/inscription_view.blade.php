@extends('template')
@section('contenu')
    <div class="container-fluid">
        <div class="col-sm-12 inscription_form">
            <div class="panel panel-default">
                <div class="panel-heading"><i class=" fa fa-angellist"></i><h4>Création de compte</h4></div>

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('nom') ? ' has-error' : '' }}">
                            <div class="col-sm-10 col-sm-offset-1">
                                <input id="nom" type="text" class="form-control firstinput" name="nom"
                                       value="{{ old('nom') }}"
                                       required autofocus placeholder="Nom">

                                @if ($errors->has('nom'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('nom') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('prenom') ? ' has-error' : '' }}">
                            <div class="col-sm-10 col-sm-offset-1">
                                <input id="prenom" type="text" class="form-control" name="prenom"
                                       value="{{ old('prenom') }}"
                                       required autofocus placeholder="Prénom">

                                @if ($errors->has('prenom'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('prenom') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('mail') ? ' has-error' : '' }}">
                            <div class="col-sm-10 col-sm-offset-1">
                                <input id="mail" type="email" class="form-control" name="mail" value="{{ old('mail') }}"
                                       required autofocus placeholder="Mail">

                                @if ($errors->has('mail'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('mail') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('telephone') ? ' has-error' : '' }}">
                            <div class="col-sm-10 col-sm-offset-1">
                                <input id="telephone" type="email" class="form-control" name="telephone"
                                       value="{{ old('telephone') }}"
                                       required autofocus placeholder="Télephone">

                                @if ($errors->has('telephone'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('telephone') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">

                            <div class="col-sm-10 col-sm-offset-1">
                                <input id="password" type="password" class="form-control" name="password" required
                                       placeholder="Mot de passe">

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-10 col-sm-offset-1">
                                <input id="password-confirm" type="password" class="form-control"
                                       name="password_confirmation" required placeholder="Confirmer mot de passe">
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('photo') ? ' has-error' : '' }}">

                            <div class="col-sm-10 col-sm-offset-1">
                                <input id="photo" type="file" class="form-control" name="photo" required
                                       placeholder="Photo">

                                @if ($errors->has('photo'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('photo') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-10 col-sm-offset-1">
                                <div class="input_radio">
                                    <div class="form-check">
                                        <input class="form-check-input" name="type_compte" type="radio"
                                               id="type_compte">
                                        <label class="form-check-label" for="type_compte">Elève</label>
                                    </div>

                                    <div class="form-check">
                                        <input class="form-check-input" name="type_compte" type="radio" id="type_compte"
                                               checked>
                                        <label class="form-check-label" for="type_compte">Moniteur</label>
                                    </div>

                                </div>

                            </div>
                        </div>
                        <div class="moniteur_attribute">
                            <div class="form-group{{ $errors->has('permis_conduire') ? ' has-error' : '' }}">

                                <div class="col-sm-10 col-sm-offset-1">
                                    <input id="permis" type="file" class="form-control" name="permis" required
                                           placeholder="Permis de conduire">

                                    @if ($errors->has('permis_conduire'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('permis_conduire')}}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('autorisation_enseigner') ? ' has-error' : '' }}">

                                <div class="col-sm-10 col-sm-offset-1">
                                    <input id="autorisation_enseigner" type="file" class="form-control"
                                           name="autorisation_enseigner" required
                                           placeholder="Autorisation d'enseigner">
                                    @if ($errors->has('autorisation_enseigner'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('autorisation_enseigner')}}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('carte_grise') ? ' has-error' : '' }}">

                                <div class="col-sm-10 col-sm-offset-1">
                                    <input id="carte_grise" type="file" class="form-control" name="carte_grise" required
                                           placeholder="Carte grise véhicule">
                                    @if ($errors->has('carte_grise'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('carte_grise')}}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('repertoire_sirene') ? ' has-error' : '' }}">

                                <div class="col-sm-10 col-sm-offset-1">
                                    <input id="repertoire_sirene" type="file" class="form-control"
                                           name="repertoire_sirene" required placeholder="Demande repértoire SIRENE">
                                    @if ($errors->has('repertoire_sirene'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('repertoire_sirene')}}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                        </div>
                        <div class="eleve_attribute">
                            <div class="form-group{{ $errors->has('repertoire_sirene') ? ' has-error' : '' }}">

                                <div class="col-sm-10 col-sm-offset-1">
                                    <select class="selectpicker" name="inscription_ants">
                                        <option>En cours</option>
                                        <option>Validé</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('numero_neph') ? ' has-error' : '' }}">
                                <div class="col-sm-10 col-sm-offset-1">
                                    <input id="numero_neph" type="text" class="form-control" name="numero_neph"
                                           value="{{ old('numero_neph') }}"
                                           required autofocus placeholder="Numéro NEPH">
                                    @if ($errors->has('numero_neph'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('numero_neph') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-6 col-sm-offset-3 connexion">
                                <button type="submit" class="btn btn-primary">
                                    Terminer
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')

@endsection
