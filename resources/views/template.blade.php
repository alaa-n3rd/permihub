<!DOCTYPE html>
<html lang="fr">
    <head>
        <!-- meta -->
        @include('layouts.meta')
        <!-- endmeta -->
        <!-- link -->
    	@include('layouts.link_style')
    	<!-- end link -->
	</head>
	<body  id="_page">
        <header id="header-arc-en-chine">
            <div class="block-header">

            </div>

        </header>
        

        @yield('contenu')
        <footer id="footer-arc-en-chine">
            <div class="block-footer container">
                <div class="col-sm-2 col-md-2 col-md-12">
                </div>
                <div class="col-sm-3 col-md-3 col-md-12">
                    <ul class="block-cms">

                    </ul>
                </div>
                <div class="col-sm-2 col-md-2 col-md-12">
                    <ul class="block-cms">

                    </ul>
                </div>
                <div class="col-sm-2 col-md-2 col-md-12">
                    <ul class="block-cms">

                    </ul>
                </div>
                <div class="col-sm-3 col-md-3 col-md-12">
                    <ul class="block-sociallink">

                    </ul>
                </div>
                <div id="bloc-links" class="container">
                    <div class="col-sm-12 col-md-12 col-md-12 list-items-footer">
                    <ul>

                    </ul>
                    </div>
                </div>
            </div>
        </footer>
        @yield('modal')

        @include('layouts.script')

        @yield('script')
        

	</body>
</html>