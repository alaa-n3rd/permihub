<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/
Route::get('/dashbord',['uses'=>'HomeController@index','as'=>'home']);
Route::get('/login',['uses'=>'HomeController@login','as'=>'login']);
Route::get('/inscription',['uses'=>'HomeController@inscription','as'=>'inscription']);



/*Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');*/
