<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Config;
use App;
use View;
use stdClass;
use DB;
use Session;
use Lang;

class HomeController extends Controller
{


    public function __construct()
    {

    }

    public function index()
    {

        $data['current_page'] = 'dashbord';
        $session_customers = Session::get('session_customers');
        if (empty($session_customers)) {
            return redirect()->route('login');
        }
        $data['session_customers'] = $session_customers;
        if ($session_customers->customers_types == 'students') {
            return View::make('students.dashbord_view', $data);
        } else if ($session_customers->customers_types == 'monitors') {
            return View::make('monitors.dashbord_view', $data);
        } else {
            Session::flush('session_customers');
            return redirect()->route('login');
        }


    }

    public function login()
    {
        $data['current_page'] = 'login';
        return View::make('login_view', $data);
    }
    public function inscription()
    {
        $data['current_page'] = 'inscription';
        return View::make('inscription_view', $data);
    }

    public function AllBlogs()
    {

        $language = Session::get('applocale');
        if (empty($language)) $language = 'fr';
        App::setLocale($language);

        $metaData = new stdClass();
        $metaData->title = Lang::get('meta.all_blog_title');
        $metaData->keywords = 'Arc En Chine';
        $metaData->description = Lang::get('meta.all_blog_description');
        $data['metaData'] = $metaData;
        $data['current_page'] = 'blogs';
        $session_customers = Session::get('session_customers');
        $session_customers_students = Session::get('session_customers_students');

        $commonModel = new CommonModel;
        $courses_types = $commonModel->simpleListing2languages('courses_types', 'courses_types_id', 'DESC');
        $blogs = $commonModel->simpleListing2languages('blogs', 'blogs_sort_order', 'DESC');
        // print_r($blogs); exit;
        $data['session_customers'] = $session_customers;
        $data['session_customers_students'] = $session_customers_students;
        $data['courses_types'] = $courses_types;
        $data['blogs'] = $blogs;
        return View::make('blogs.all_blogs_view', $data);

    }

    public function DetailBlogs($blogs_id, $blogs_name)
    {

        $language = Session::get('applocale');
        if (empty($language)) $language = 'fr';
        App::setLocale($language);

        $metaData = new stdClass();
        $metaData->title = Lang::get('meta.default_title');
        $metaData->keywords = 'Arc En Chine';
        $metaData->description = Lang::get('meta.default_description');
        $data['metaData'] = $metaData;
        $data['current_page'] = 'blogs';
        $session_customers = Session::get('session_customers');
        $session_customers_students = Session::get('session_customers_students');

        $commonModel = new CommonModel;
        $courses_types = $commonModel->simpleListing2languages('courses_types', 'courses_types_id', 'DESC');
        list($sql, $blog) = $commonModel->getElement2languagesById('blogs', 'blogs_id', $blogs_id);
        if (empty($blog))
            return route('home');
        // print_r($blog); exit;
        $data['session_customers'] = $session_customers;
        $data['session_customers_students'] = $session_customers_students;
        $data['courses_types'] = $courses_types;
        $data['blog'] = $blog;
        return View::make('blogs.blogs_view', $data);

    }

    public function LinkAppearin()
    {

        $language = Session::get('applocale');
        if (empty($language)) $language = 'fr';
        App::setLocale($language);

        $metaData = new stdClass();
        $metaData->title = Lang::get('meta.default_title');
        $metaData->keywords = 'Arc En Chine';
        $metaData->description = Lang::get('meta.default_description');
        $data['metaData'] = $metaData;
        $data['current_page'] = 'blogs';
        $session_customers = Session::get('session_customers');
        $session_customers_students = Session::get('session_customers_students');
        $commonModel = new CommonModel;
        $courses_types = $commonModel->simpleListing2languages('courses_types', 'courses_types_id', 'DESC');

        // print_r($blog); exit;
        $data['session_customers'] = $session_customers;
        $data['session_customers_students'] = $session_customers_students;
        $data['courses_types'] = $courses_types;
        return View::make('appear_in_view', $data);

    }

    public function CGV()
    {
        $language = Session::get('applocale');
        if (empty($language)) $language = 'fr';
        App::setLocale($language);

        $metaData = new stdClass();
        $metaData->title = Lang::get('meta.default_title');
        $metaData->keywords = 'Arc En Chine';
        $metaData->description = Lang::get('meta.default_description');
        $data['metaData'] = $metaData;
        $data['current_page'] = 'cgv';
        $session_customers = Session::get('session_customers');
        $session_customers_students = Session::get('session_customers_students');

        $commonModel = new CommonModel;
        $courses_types = $commonModel->simpleListing2languages('courses_types', 'courses_types_id', 'DESC');

        list($sql, $data['cgv']) = $commonModel->getElement2languagesById('static_contents', 'static_contents_id', '7');

        $data['session_customers'] = $session_customers;
        $data['session_customers_students'] = $session_customers_students;
        $data['courses_types'] = $courses_types;
        return view('cgv', $data);

    }

    public function LegalNotice()
    {
        $language = Session::get('applocale');
        if (empty($language)) $language = 'fr';
        App::setLocale($language);

        $metaData = new stdClass();
        $metaData->title = Lang::get('meta.default_title');
        $metaData->keywords = 'Arc En Chine';
        $metaData->description = Lang::get('meta.default_description');
        $data['metaData'] = $metaData;
        $data['current_page'] = 'legal_notice';
        $session_customers = Session::get('session_customers');
        $session_customers_students = Session::get('session_customers_students');

        $commonModel = new CommonModel;
        $courses_types = $commonModel->simpleListing2languages('courses_types', 'courses_types_id', 'DESC');
        list($sql, $data['legal_notice']) = $commonModel->getElement2languagesById('static_contents', 'static_contents_id', '8');

        $data['session_customers'] = $session_customers;
        $data['session_customers_students'] = $session_customers_students;
        $data['courses_types'] = $courses_types;
        return view('legal_notice', $data);

    }

    public function ForgetPassword()
    {
        $language = Session::get('applocale');
        if (empty($language)) $language = 'fr';
        App::setLocale($language);

        $metaData = new stdClass();
        $metaData->title = Lang::get('meta.default_title');
        $metaData->keywords = 'Arc En Chine';
        $metaData->description = Lang::get('meta.default_description');
        $data['metaData'] = $metaData;
        $data['current_page'] = 'forget_password';
        $session_customers = Session::get('session_customers');
        $session_customers_students = Session::get('session_customers_students');

        //$user = AecCustomers::where('customers_email','=',Input::get('email'))->first();print_r($user->customers_firstname); exit;

        $commonModel = new CommonModel;
        $courses_types = $commonModel->simpleListing2languages('courses_types', 'courses_types_id', 'DESC');
        list($sql, $data['legal_notice']) = $commonModel->getElement2languagesById('static_contents', 'static_contents_id', '8');

        $data['session_customers'] = $session_customers;
        $data['session_customers_students'] = $session_customers_students;
        $data['courses_types'] = $courses_types;
        return view('forget_password', $data);
    }

    public function FAQ()
    {
        $language = Session::get('applocale');
        if (empty($language)) $language = 'fr';
        App::setLocale($language);

        $metaData = new stdClass();
        $metaData->title = Lang::get('meta.default_title');
        $metaData->keywords = 'Arc En Chine';
        $metaData->description = Lang::get('meta.default_description');
        $data['metaData'] = $metaData;
        $data['current_page'] = 'faq';
        $session_customers = Session::get('session_customers');
        $session_customers_students = Session::get('session_customers_students');

        $commonModel = new CommonModel;
        $courses_types = $commonModel->simpleListing2languages('courses_types', 'courses_types_id', 'DESC');

        //list($sql,$data['faq']) = $commonModel->getElement2languagesById('static_contents','static_contents_id','7');

        $data['session_customers'] = $session_customers;
        $data['session_customers_students'] = $session_customers_students;
        $data['courses_types'] = $courses_types;
        return view('faq', $data);

    }

    public function About()
    {
        $language = Session::get('applocale');
        if (empty($language)) $language = 'fr';
        App::setLocale($language);

        $metaData = new stdClass();
        $metaData->title = Lang::get('meta.about_title');
        $metaData->keywords = 'Arc En Chine';
        $metaData->description = Lang::get('meta.about_description');
        $data['metaData'] = $metaData;
        $data['current_page'] = 'about';
        $session_customers = Session::get('session_customers');
        $session_customers_students = Session::get('session_customers_students');

        $commonModel = new CommonModel;
        $courses_types = $commonModel->simpleListing2languages('courses_types', 'courses_types_id', 'DESC');

        list($sql, $data['apropos']) = $commonModel->getElement2languagesById('static_contents', 'static_contents_id', '13');

        $data['session_customers'] = $session_customers;
        $data['session_customers_students'] = $session_customers_students;
        $data['courses_types'] = $courses_types;
        return view('about', $data);

    }

    public function SitePlan()
    {
        $language = Session::get('applocale');
        if (empty($language)) $language = 'fr';
        App::setLocale($language);

        $metaData = new stdClass();
        $metaData->title = Lang::get('meta.default_title');
        $metaData->keywords = 'Arc En Chine';
        $metaData->description = Lang::get('meta.default_description');
        $data['metaData'] = $metaData;
        $data['current_page'] = 'plan_site';
        $session_customers = Session::get('session_customers');
        $session_customers_students = Session::get('session_customers_students');

        $commonModel = new CommonModel;
        $courses_types = $commonModel->simpleListing2languages('courses_types', 'courses_types_id', 'DESC');

        //list($sql,$data['faq']) = $commonModel->getElement2languagesById('static_contents','static_contents_id','7');

        $data['session_customers'] = $session_customers;
        $data['session_customers_students'] = $session_customers_students;
        $data['courses_types'] = $courses_types;
        return view('plan_site', $data);

    }

    public function News()
    {
        $language = Session::get('applocale');
        if (empty($language)) $language = 'fr';
        App::setLocale($language);

        $metaData = new stdClass();
        $metaData->title = Lang::get('meta.default_title');
        $metaData->keywords = 'Arc En Chine';
        $metaData->description = Lang::get('meta.default_description');
        $data['metaData'] = $metaData;
        $data['current_page'] = 'news';
        $session_customers = Session::get('session_customers');
        $session_customers_students = Session::get('session_customers_students');

        $commonModel = new CommonModel;
        $courses_types = $commonModel->simpleListing2languages('courses_types', 'courses_types_id', 'DESC');

        //list($sql,$data['faq']) = $commonModel->getElement2languagesById('static_contents','static_contents_id','7');

        $data['session_customers'] = $session_customers;
        $data['session_customers_students'] = $session_customers_students;
        $data['courses_types'] = $courses_types;
        return view('news', $data);

    }


    public function detailContent($nom, $id)
    {
        $language = Session::get('applocale');
        if (empty($language)) $language = 'fr';
        App::setLocale($language);

        $metaData = new stdClass();
        $metaData->title = ($id == 9 ? Lang::get('meta.detail_content_9_title') : Lang::get('meta.detail_content_10_title'));
        $metaData->keywords = 'Arc En Chine';
        $metaData->description = ($id == 9 ? Lang::get('meta.detail_content_9_description') : Lang::get('meta.detail_content_10_description'));
        $data['metaData'] = $metaData;
        $data['current_page'] = 'news';
        $session_customers = Session::get('session_customers');
        $session_customers_students = Session::get('session_customers_students');


        $commonModel = new CommonModel;
        list($sql, $data['content']) = $commonModel->getElement2languagesById('static_contents', 'static_contents_id', $id);
        //$courses_types = $commonModel->simpleListing2languages('courses_types','courses_types_id','DESC');

        $data['session_customers'] = $session_customers;
        $data['session_customers_students'] = $session_customers_students;

        return view('home.detailcontent', $data);

    }


}


?>
